from .app import app
from flask import render_template
from .models import get_sample

@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Hello world avec template",
        names=["Pierre","Paul","Jacques"]
    ) 

#Exercice 7
@app.route("/books")
def books():
    return render_template(
        "books.html",
        title = 'Liste de livres',
        books = get_sample()
    )